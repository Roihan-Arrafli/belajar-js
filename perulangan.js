const listSiswa = [
    {
        nama: "Roihan Arrafli",
        umur: 17,
        hobby: ["Main Game", "Tidur"]
    },
    {
        nama: "Raya Irfansyah",
        umur: 18,
        hobby: ["Main Game", "Baca Manga"]
    },
    {
        nama: "Angga Maulana Efendi",
        umur: 19,
        hobby: ["Main Game", "Nonton Anime"]
    },
    {
        nama: "Muhammad Abyan Al Ghifari",
        umur: 18,
        hobby: ["Menggambar", "Main Game"]
    },
    {
        nama: "Hanifah Siti Jaga  Pramudya",
        umur: 18,
        hobby: ["Badminton", "Membaca"]
    },
    {
        nama: "Kafita Putri Rusdwiana",
        umur: 17,
        hobby: ["Memasak", "Membaca"]
    },
    {
        nama: "Renata Kurniawati",
        umur: 17,
        hobby: ["Membaca Novel", "Jalan-Jalan"]
    },
    {
        nama: "Riska Lailatus Sholikha",
        umur: 17,
        hobby: ["Membaca", "Menulis"]
    }
];

//menampilkan semua nama
// for(let i = 0; i <listSiswa.length; i++){
//     console.log(listSiswa[i]["nama"]);
// }

function pencarianNama(nama) {
    for(let i = 0; i <listSiswa.length; i++){
        if(listSiswa[i]["nama"] == nama){
            console.log(listSiswa[i]["nama"]);
        }
    }
}
pencarianNama("Roihan Arrafli");

// filter umur
function filterUmur(umur) {
    for(let i = 0; i < listSiswa.length; i++){
        if(listSiswa[i]["umur"] == umur){
            console.log(listSiswa[i]["nama"]);
        }
    }
}
filterUmur(17);