const nilai1 = 2;
const nilai2 = 4;

//function deklarasi

// penjumlahan
function penjumlahan(a, b){
    console.log(a+b);
}

penjumlahan(nilai1, nilai2);

// perkalian
function perkalian(a, b){
    console.log(a*b);
}

perkalian(nilai1, nilai2);

//pengurangan
function pengurangan(a, b){
    console.log(a-b);
}

pengurangan(nilai1, nilai2);

//pembagian 
function pembagian(a, b){
    console.log(a/b);
}

pembagian(nilai1, nilai2);

//rata-rata
function rerata(a,b){
    console.log((a+b)/2);
}

rerata(nilai1, nilai2);

// function ekspresi
const sayHi = function(nama){
    console.log("Halo " + nama);
}

sayHi("Roihan");

// bagi
const bagi = function(nilai3, nilai4 ){
    console.log("Hasilnya " + nilai3 / nilai4 );
}

bagi(6, 2);

// jumlah
const jumlah = function(nilai5, nilai6){
    console.log("Hasilnya " + (nilai5+nilai6) )
}

jumlah(5, 5);

// kurang
const kurang = function(nilai7, nilai8) {
    console.log("Hasilnya " + (nilai7-nilai8));
}

kurang(10, 5);

// kali
const kali = function(nilai9, nilai10){
    console.log("Hasilnya " + (nilai9*nilai10));
}

kali(5, 5);

// rata
const rata = function(nilai9, nilai10){
    console.log("Hasilnya " + (nilai9+nilai10)/2);
}

rata(10, 2);