//
const profile = {
    nama: "Roihan",
    hobby: ["Mancing", "Makan"]
}

console.log(profile.nama, profile.hobby[0]);

//
const profilku = {
    nama: "Arrafli",
    hobby: ["a", "b"]
}

console.log(profilku["nama"], profilku["hobby"][1]);