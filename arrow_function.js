//tambah
let sum = (a, b) => a+b;
console.log(sum(3, 2));

//kurang
let min = (a,b) => a-b;
console.log(min(10, 5));

//kali
let kali = (a,b) => a*b;
console.log(kali(2, 5));

//bagi
let bagi = (a,b) => a/b;
console.log(bagi(20, 2));

//rata-rata
let rata = (a,b) => (a+b)/2;
console.log(rata(20, 10));

const nilai1 = 5;
const nilai2 = 3;

function dijumlah(a, b){
    return a+b;
}

const result = dijumlah(nilai1, nilai2);
console.log(result);