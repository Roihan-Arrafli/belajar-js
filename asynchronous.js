// // menggunakan function biasa
// console.log("Hi Roihan");
// setTimeout(function () {
//     console.log("the time has come");
// }, 3000);
// console.log("to learn javascript");

// // menggunakan arrow function
// console.log("Hello");
// setTimeout(() => {
//     console.log("Javascript");
// },100) // tunda selama 100 miliseconds
// console.log("Coder");

//
console.log("Roihan Arrafli");
setTimeout( () => {
    console.log("Angga");
}, 3000);
console.log("Maulana Efendi");